package com.infy.tele.repository;

import com.infy.tele.domain.Emp;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Emp entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EmpRepository extends JpaRepository<Emp, Long> {

}
